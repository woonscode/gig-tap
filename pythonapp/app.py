from flask import Flask, render_template
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app)

@app.route("/")
def home():
    return render_template("index.html", color="green")

@app.route("/hello")
def hello():
    return f"Hello!"

@app.route("/exit")
def bye():
    return f"Goodbye!"

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)