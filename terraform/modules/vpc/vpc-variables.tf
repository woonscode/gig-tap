variable "vpc_name" {
  description = "Name of VPC"
  type = string
  default = "gig-app-vpc"
}

variable "public_subnet_1_name" {
  description = "Name of 1st public subnet"
  type = string
  default = "gig-app-public-subnet-1"
}

variable "public_subnet_2_name" {
  description = "Name of 2nd public subnet"
  type = string
  default = "gig-app-public-subnet-2"
}

variable "igw_name" {
  description = "Name of Internet Gateway"
  type = string
  default = "gig-app-igw"
}

variable "public_rt_name" {
  description = "Name of public route table"
  type = string
  default = "gig-app-public-rt"
}