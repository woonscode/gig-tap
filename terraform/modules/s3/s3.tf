resource "aws_s3_bucket" "alb_access_logs" {
  bucket        = "gig-app-alb-logs"
  force_destroy = true
}

resource "aws_s3_bucket_policy" "alb_policy" {
  bucket = aws_s3_bucket.alb_access_logs.id
  policy = file("./policies/alb-logs-bucket-policy.json")
}

resource "aws_s3_bucket_server_side_encryption_configuration" "alb_encryption" {
  bucket = aws_s3_bucket.alb_access_logs.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "aws:kms"
    }
  }

  depends_on = [aws_s3_bucket_policy.alb_policy]
}

resource "aws_s3_bucket_versioning" "alb_versioning" {
  bucket = aws_s3_bucket.alb_access_logs.id

  versioning_configuration {
    status = "Enabled"
  }
  depends_on = [aws_s3_bucket_server_side_encryption_configuration.alb_encryption]
}