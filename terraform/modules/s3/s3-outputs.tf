output "alb_log_bucket_policy_bucket_name" {
  description = "Name of bucket for ALB access logs"
  value       = aws_s3_bucket_policy.alb_policy.bucket
}