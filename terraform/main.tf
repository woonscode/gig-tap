terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.17.0"
    }
  }

# ENV variables for access_key and secret_key in pipeline
  backend "s3" {
    bucket = "gig-app-wh"
    key    = "terraform/state"
    region = "ap-southeast-1"
  }
}

# ENV variables for access_key and secret_key in pipeline
provider "aws" {
  region = "ap-southeast-1"
}

module "s3" {
  source = "./modules/s3"
}

module "vpc" {
  source = "./modules/vpc"
}

resource "aws_iam_role" "asg_instance_role" {
  name                = "asg-instance-role"
  assume_role_policy  = file("./policies/asg-instance-role-trust-policy.json")
  managed_policy_arns = var.asg_instance_role_managed_policies
}

resource "aws_iam_instance_profile" "asg_instance_profile" {
  name = "asg-instance-profile"
  role = aws_iam_role.asg_instance_role.id
}

resource "aws_security_group" "asg_instance_sg" {
  name        = "asg-instance-sg"
  description = "Security group for instances in the ASG"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description      = "default inbound rule"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    description      = "default outbound rule"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.instance_sg_name
  }
}

resource "aws_security_group" "alb_sg" {
  name        = "alb-sg"
  description = "Security group for ALB"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description      = "default inbound rule"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    description      = "default outbound rule"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.alb_sg_name
  }
}

resource "aws_launch_template" "web_servers" {
  name                   = "gig-app-LT"
  description            = "Main launch template for use with ASG"
  # ECS optimised Amazon Linux AMI - amzn2-ami-ecs-hvm-2.0.20230906-x86_64-ebs
  image_id               = "ami-09b722696225886f8"
  instance_type          = "t2.small"
  vpc_security_group_ids = [aws_security_group.asg_instance_sg.id]
  user_data              = filebase64("./ec2-user-data.sh")

  iam_instance_profile {
    name = aws_iam_instance_profile.asg_instance_profile.id
  }

  monitoring {
    enabled = true
  }

  tags = {
    Name = "gig-app-LT"
  }
}

resource "aws_autoscaling_group" "main_asg" {
  name                    = "gig-app-ASG"
  desired_capacity        = 2
  min_size                = 2
  max_size                = 4
  default_instance_warmup = 120
  vpc_zone_identifier     = [module.vpc.public_subnet_1_id, module.vpc.public_subnet_2_id]
  health_check_grace_period = 300
  health_check_type       = "ELB"
  protect_from_scale_in   = true
  
  launch_template {
    id      = aws_launch_template.web_servers.id
    version = "$Latest"
  }

  tag {
    key                 = "AmazonECSManaged"
    value               = true
    propagate_at_launch = true
  }
}

resource "aws_lb" "main_alb" {
  name                 = "gig-app-alb"
  internal             = false
  load_balancer_type   = "application"
  security_groups      = [aws_security_group.alb_sg.id]
  subnets              = [module.vpc.public_subnet_1_id, module.vpc.public_subnet_2_id]

  access_logs {
    bucket  = module.s3.alb_log_bucket_policy_bucket_name
    enabled = true
  }

  tags = {
    Name = "gig-app-alb"
  }
}

resource "aws_lb_target_group" "alb_target_group" {
  name        = "gig-app-target-group"
  target_type = "ip"
  port        = "5000"
  protocol    = "HTTP"
  vpc_id      = module.vpc.vpc_id

  health_check {
    enabled = true
    matcher = "200"
    path = "/"
    port = 5000
  }

}

resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.main_alb.arn
  port              = "5000"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_target_group.arn
  }
}

resource "aws_iam_role" "ecs_tasks_role" {
  name                = "ecs-tasks-role"
  assume_role_policy  = file("./policies/ecs-tasks-role-trust-policy.json")
}

resource "aws_iam_instance_profile" "ecs_tasks_profile" {
  name = "ecs-tasks-profile"
  role = aws_iam_role.ecs_tasks_role.id
}

resource "aws_ecs_cluster" "app" {
  name = "gig-app"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }

  tags = {
    Name = "gig-app"
  }
}

resource "aws_ecs_cluster_capacity_providers" "app" {
  cluster_name = aws_ecs_cluster.app.name

  capacity_providers = [aws_ecs_capacity_provider.app.name]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = aws_ecs_capacity_provider.app.name
  }
}

resource "aws_ecs_capacity_provider" "app" {
  name = "gig-app"

  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.main_asg.arn
    managed_termination_protection = "ENABLED"

    managed_scaling {
      instance_warmup_period    = 180
      maximum_scaling_step_size = 10000
      minimum_scaling_step_size = 1
      status                    = "ENABLED"
      target_capacity           = 100
    }
  }
}

resource "aws_ecs_task_definition" "app" {
  family                = "app"
  network_mode          = "awsvpc"
  task_role_arn         = aws_iam_role.ecs_tasks_role.arn
  container_definitions = jsonencode([
    {
      name      = "app"
      image     = "611343350211.dkr.ecr.ap-southeast-1.amazonaws.com/gig-app:latest"
      cpu       = 1
      memory    = 128
      essential = true
      portMappings = [
        {
          containerPort = 5000
          hostPort      = 5000
        }
      ]
    }
  ])
}

resource "aws_ecs_service" "app" {
  name                              = "app"
  cluster                           = aws_ecs_cluster.app.id
  desired_count                     = 2
  force_new_deployment              = true
  health_check_grace_period_seconds = 300
  launch_type                       = "EC2"
  task_definition                   = aws_ecs_task_definition.app.family

  ordered_placement_strategy {
    type  = "spread"
    field = "instanceId"
  }

  network_configuration {
    subnets         = [module.vpc.public_subnet_1_id, module.vpc.public_subnet_2_id]
    security_groups = [aws_security_group.asg_instance_sg.id]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.alb_target_group.arn
    container_name   = "app"
    container_port   = 5000
  }
}

