variable "asg_instance_role_managed_policies" {
  description = "List of AWS managed policies to be attached to ECS host instances"
  type = list
  default = [
              "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role",
  ]
}

variable "instance_sg_name" {
  description = "Name of security group to associate with instances in ASG"
  type = string
  default = "gig-instance-sg"
}

variable "alb_sg_name" {
  description = "Name of security group to associate with ALB"
  type = string
  default = "gig-alb-sg"
}