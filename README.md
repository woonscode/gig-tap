# gig-tap
Code repository for the technical assessment for GIG for Yong Woon Hao

## Deployment

The Terraform code for the application requires the following AWS resources set up first:

* An S3 bucket to store the backend state file for Terraform
* An ECR repository to store the image built in the pipeline

As such, the Terraform code to set up the above resources have been separated to the ```terraform/init/init.tf``` file. This file only has to be run **once**.

To set up:

1. Navigate to the ```terraform/init``` directory and run ```terraform init``` and ```terraform apply --auto-approve``` locally. *Note: AWS credentials for your account have to be set up prior to running the Terraform commands*

2. Trigger the pipeline to set up the remaining infrastructure for the application.

